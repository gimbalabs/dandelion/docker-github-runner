FROM ubuntu:20.04

WORKDIR /opt
VOLUME ["/opt/_work", "/opt/_diag"]
ENV APT_ARGS="-y -o APT::Install-Suggests=false -o APT::Install-Recommends=false"
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -qq && apt install ${APT_ARGS} curl git-lfs sudo ca-certificates
RUN mkdir -p /nonexistent /data && \
    chown nobody: /nonexistent

ARG GH_RUNNER_VERSION=2.291.1
ENV GH_RUNNER_URL=https://github.com/actions/runner/releases/download/v${GH_RUNNER_VERSION}/actions-runner-linux-x64-${GH_RUNNER_VERSION}.tar.gz

RUN curl -o /tmp/gh-runner.tgz -L https://github.com/actions/runner/releases/download/v${GH_RUNNER_VERSION}/actions-runner-linux-x64-${GH_RUNNER_VERSION}.tar.gz && \
    tar -zxf /tmp/gh-runner.tgz -C /opt && \
    /opt/bin/installdependencies.sh && \
    chown -R nobody: /opt
    
ENTRYPOINT ["bash", "-xc", "sudo chown -R nobody: /opt; sudo -u nobody ./config.sh --unattended --name ${RUNNER_NAME} --url ${RUNNER_URL} --token ${RUNNER_TOKEN}; sudo -u nobody ./run.sh"]
